declare module "react-native-twilio-video-webrtc" {
  import { ViewProps } from "react-native";
  import React from "react";

  export type BandwidthProfileApi = {
    mode: "grid" | "collaboration" | "presentation";
    trackSwitchOffMode?: "predicted" | "detected" | "disabled";
    maxSubscriptionBitrate?: number;
    dominantSpeakerPriority?: "high" | "low" | "standard";
    clientTrackSwitchOffControl?: "auto" | "manual";
    contentPreferencesMode?: "auto" | "manual";
  };

  export interface TrackIdentifier {
    participantSid: string;
    videoTrackSid: string;
  }

  type scaleType = "fit" | "fill";
  type cameraType = "front" | "back";

  interface TwilioVideoParticipantViewProps extends ViewProps {
    trackIdentifier: TrackIdentifier;
    ref?: React.Ref<any>;
    scaleType?: scaleType;
  }

  interface TwilioVideoLocalViewProps extends ViewProps {
    enabled: boolean;
    ref?: React.Ref<any>;
    scaleType?: scaleType;
  }

  export interface Participant {
    sid: string;
    identity: string;
  }

  export interface Track {
    enabled: boolean;
    trackName: string;
    trackSid: string;
  }

  export interface TrackEventCbArgs {
    participant: Participant;
    track: Track;
  }

  export type TrackEventCb = (t: TrackEventCbArgs) => void;

  export interface DataTrackEventCbArgs {
    message: string;
    trackSid: string;
  }

  export type DataTrackEventCb = (t: DataTrackEventCbArgs) => void;

  interface RoomEventCommonArgs {
    roomName: string;
    roomSid: string;
  }

  export type RoomErrorEventArgs = RoomEventCommonArgs & {
    error: any;
  };

  type RoomEventArgs = RoomEventCommonArgs & {
    participants: Participant[];
    localParticipant: Participant;
  };

  type ParticipantEventArgs = RoomEventCommonArgs & {
    participant: Participant;
  };

  type NetworkLevelChangeEventArgs = {
    participant: Participant;
    isLocalUser: boolean;
    quality: number;
  };

  type DominantSpeakerChangeArgs = RoomEventCommonArgs & {
    participant: Participant | String;
  };

  export type RoomEventCb = (p: RoomEventArgs) => void;
  export type RoomErrorEventCb = (t: RoomErrorEventArgs) => void;

  export type ParticipantEventCb = (p: ParticipantEventArgs) => void;

  export type DominantSpeakerChangeEventCb = (
    p: DominantSpeakerChangeArgs
  ) => void;

  export type NetworkLevelChangeEventCb = (
    p: NetworkLevelChangeEventArgs
  ) => void;

  export type TwilioVideoProps = ViewProps & {
    onCameraDidStart?: () => void;
    onCameraDidStopRunning?: (err: any) => void;
    onCameraWasInterrupted?: () => void;
    onParticipantAddedAudioTrack?: TrackEventCb;
    onParticipantAddedVideoTrack?: TrackEventCb;
    onParticipantDisabledVideoTrack?: TrackEventCb;
    onParticipantDisabledAudioTrack?: TrackEventCb;
    onParticipantEnabledVideoTrack?: TrackEventCb;
    onParticipantEnabledAudioTrack?: TrackEventCb;
    onParticipantRemovedAudioTrack?: TrackEventCb;
    onParticipantRemovedVideoTrack?: TrackEventCb;
    onParticipantAddedDataTrack?: TrackEventCb;
    onParticipantRemovedDataTrack?: TrackEventCb;
    onRoomDidConnect?: RoomEventCb;
    onRoomDidDisconnect?: RoomErrorEventCb;
    onRoomDidFailToConnect?: RoomErrorEventCb;
    onRoomParticipantDidConnect?: ParticipantEventCb;
    onRoomParticipantDidDisconnect?: ParticipantEventCb;
    onNetworkQualityLevelsChanged?: NetworkLevelChangeEventCb;
    onDominantSpeakerDidChange?: DominantSpeakerChangeEventCb;
    onStatsReceived?: (data: any) => void;
    onDataTrackMessageReceived?: DataTrackEventCb;
    ignoreVideo?: boolean;
    ref?: React.Ref<any>;
  };

  type iOSConnectParams = {
    roomName?: string;
    accessToken: string;
    cameraType?: cameraType;
    enableAudio?: boolean;
    enableVideo?: boolean;
    encodingParameters?: {
      enableH264Codec?: boolean;
      // if audioBitrate OR videoBitrate is provided, you must provide both
      audioBitrate?: number;
      videoBitrate?: number;
    };
    enableNetworkQualityReporting?: boolean;
    dominantSpeakerEnabled?: boolean;
    bandwidthProfileApi?: BandwidthProfileApi;
    ignoreVideo?: boolean;
  };

  type androidConnectParams = {
    roomName?: string;
    accessToken: string;
    cameraType?: cameraType;
    enableAudio?: boolean;
    enableVideo?: boolean;
    enableRemoteAudio?: boolean;
    enableNetworkQualityReporting?: boolean;
    maintainVideoTrackInBackground?: boolean;
    dominantSpeakerEnabled?: boolean;
    bandwidthProfileApi?: BandwidthProfileApi;
    ignoreVideo?: boolean;
  };

  class TwilioVideo extends React.Component<TwilioVideoProps> {
    setLocalVideoEnabled: (enabled: boolean) => Promise<boolean>;
    setLocalAudioEnabled: (enabled: boolean) => Promise<boolean>;
    setRemoteAudioEnabled: (enabled: boolean) => Promise<boolean>;
    setBluetoothHeadsetConnected: (enabled: boolean) => Promise<boolean>;
    connect: (options: iOSConnectParams | androidConnectParams) => void;
    disconnect: () => void;
    flipCamera: () => void;
    toggleSoundSetup: (speaker: boolean) => void;
    getStats: () => void;
    publishLocalAudio: () => void;
    unpublishLocalAudio: () => void;
    publishLocalVideo: () => void;
    unpublishLocalVideo: () => void;
    sendString: (message: string) => void;
    initLocalMedia: () => void;
  }

  class TwilioVideoLocalView extends React.Component<TwilioVideoLocalViewProps> {}

  class TwilioVideoParticipantView extends React.Component<TwilioVideoParticipantViewProps> {}

  export { TwilioVideoLocalView, TwilioVideoParticipantView, TwilioVideo };
}
